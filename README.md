# Disroot-Project
Welcome to Disroot’s Project board.


This is our main board where we keep track of problems, feedback, suggestions and feature requests. It is also the place where we discuss the current progress on things and coordinate our work.


This isn’t the only board we are currently using, but we intend to make it a central point where one can see what is currently going on and engage with the project. Since most of the things we are busy with are here on this git instance, there are more project focus boards you can find around that are run by us, such as ansible roles repositories, website and how to's, etc. However this board should be your first stop.


The usage is quite simple. Anyone can submit an issue. Whether it’s reporting a bug, suggesting a new feature or app, all of those end up on a shared list. Through the use of Labels (tags) we sort out all issues so it's easier to filter out and decide where do we need to prioritize. Anyone can later follow the development of the task and engage in the discussion around it.

Please keep this board civil and do not engage in flaming, bullying, hating or simply shouting to other people. Be nice to each other. The Terms of Service are the same ones you can read on https://disroot.org/en/tos

![](project.png)
